UC Berkeley CS 61C: Great Ideas of Computer Architecture (Machine Structures), Summer 2017

Course Website: http://inst.eecs.berkeley.edu/~cs61c/su17/

Instructors: Rebecca Herman, Steven Ho

The subjects covered in this course include: C and assembly language programming, translation of high-level programs into machine language, computer organization, caches, performance measurement, parallelism, CPU design, warehouse-scale computing, and related topics. 