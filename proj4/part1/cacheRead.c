/* Summer 2017 */
#include <stdbool.h>
#include <stdint.h>
#include "utils.h"
#include "setInCache.h"
#include "cacheRead.h"
#include "cacheWrite.h"
#include "getFromCache.h"
#include "mem.h"
#include "../part2/hitRate.h"

/*
	Takes in a cache and a block number and fetches that block of data, 
	returning it in a uint8_t* pointer.
*/
uint8_t* fetchBlock(cache_t* cache, uint32_t blockNumber) {
	uint64_t location = getDataLocation(cache, blockNumber, 0);
	uint32_t length = cache->blockDataSize;
	uint8_t* data = malloc(sizeof(uint8_t) << log_2(length));
	if (data == NULL) {
		allocationFailed();
	}
	int shiftAmount = location & 7;
	uint64_t byteLoc = location >> 3;
	if (shiftAmount == 0) {
		for (uint32_t i = 0; i < length; i++) {
			data[i] = cache->contents[byteLoc + i];
		}
	} else {
		length = length << 3;
		data[0] = cache->contents[byteLoc] << shiftAmount;
		length -= (8 - shiftAmount);
		int displacement = 1;
		while (length > 7) {
			data[displacement - 1] = data[displacement - 1] | (cache->contents[byteLoc + displacement] >> (8 - shiftAmount));
			data[displacement] = cache->contents[byteLoc + displacement] << shiftAmount;
			displacement++;
			length -= 8;
		}
		data[displacement - 1] = data[displacement - 1] | (cache->contents[byteLoc + displacement] >> (8 - shiftAmount));
	}
	return data;
}

/*
	Takes in a cache, an address, and a dataSize and reads from the cache at
	that address the number of bytes indicated by the size. If the data block 
	is already in the cache it retrieves the contents. If the contents are not
	in the cache it is read into a new slot and if necessary something is 
	evicted.
*/
uint8_t* readFromCache(cache_t* cache, uint32_t address, uint32_t dataSize) {
	reportAccess(cache);
	evictionInfo_t* eviction = findEviction(cache, address);
	uint32_t blockNum =  eviction->blockNumber;
	if (!(eviction->match)) {
		uint32_t blockStart = ((address >> log_2(cache->blockDataSize)) << log_2(cache->blockDataSize));
		uint8_t* newData = readFromMem(cache, blockStart);
		writeWholeBlock(cache, address, blockNum, newData);
		free(newData);
	}
	if (eviction->match) {
		reportHit(cache);
		uint32_t tag = extractTag(cache, eviction->blockNumber);
		uint32_t idx = extractIndex(cache, eviction->blockNumber);
		long oldLRU = getLRU(cache, eviction->blockNumber);
		updateLRU(cache, tag, idx, oldLRU);
	}
	free(eviction);
	return getData(cache, getOffset(cache, address), blockNum, dataSize);
}

/*
	Takes in a cache and an address and fetches a byte of data.
	Returns a struct containing a bool field of whether or not
	data was successfully read and the data. This field should be
	false only if there is an alignment error or there is an invalid
	address selected.
*/
byteInfo_t readByte(cache_t* cache, uint32_t address) {
	byteInfo_t retVal;
	if (!validAddresses(address, 1)){
		retVal.success = false;
	} else {
		uint8_t* readData = readFromCache(cache, address, 1);
		retVal.data = *readData;
		retVal.success = true;
		free(readData);
	}
	return retVal;
}

/*
	Takes in a cache and an address and fetches a halfword of data.
	Returns a struct containing a bool field of whether or not
	data was successfully read and the data. This field should be
	false only if there is an alignment error or there is an invalid
	address selected.
*/
halfWordInfo_t readHalfWord(cache_t* cache, uint32_t address) {
	halfWordInfo_t retVal;
	if (!validAddresses(address, 2) || (address % 2) != 0){
		retVal.success = false;
	} else {
		if (cache->blockDataSize == 1) {
			uint16_t readData = readByte(cache, address).data;
			readData = readData << 8;
			readData |= readByte(cache, address + 1).data;
			retVal.data = readData;
		} else {
			uint8_t* readData = readFromCache(cache, address, 2);
			retVal.data = 0;
			for (int i = 0; i < 2; i++) {
				retVal.data = (retVal.data << 8 ) | readData[i];
			}
			free(readData);
		}
		retVal.success = true;
	}
	return retVal;
}

/*
	Takes in a cache and an address and fetches a word of data.
	Returns a struct containing a bool field of whether or not
	data was successfully read and the data. This field should be
	false only if there is an alignment error or there is an invalid
	address selected.
*/
wordInfo_t readWord(cache_t* cache, uint32_t address) {
	wordInfo_t retVal;
	if (!validAddresses(address, 4) || (address % 4) != 0){
		retVal.success = false;
	} else {
		if (cache->blockDataSize <= 2) {
			uint32_t readData = readHalfWord(cache, address).data;
			readData = readData << 16;
			readData |= readHalfWord(cache, address + 2).data;
			retVal.data = readData;
		} else {
			uint8_t* readData = readFromCache(cache, address, 4);
			retVal.data = 0;
			for (int i = 0; i < 4; i++) {
				retVal.data = (retVal.data << 8 ) | readData[i];
			}
			free(readData);
		}
		retVal.success = true;
	}
	return retVal;
}

/*
	Takes in a cache and an address and fetches a double word of data.
	Returns a struct containing a bool field of whether or not
	data was successfully read and the data. This field should be
	false only if there is an alignment error or there is an invalid
	address selected.
*/
doubleWordInfo_t readDoubleWord(cache_t* cache, uint32_t address) {
	doubleWordInfo_t retVal;
	if (!validAddresses(address, 8) || (address % 8) != 0){
		retVal.success = false;
	} else {
		if (cache->blockDataSize <= 4) {
			uint64_t readData = readWord(cache, address).data;
			readData = readData << 32;
			readData |= readWord(cache, address + 4).data;
			retVal.data = readData;
		} else {
			uint8_t* readData = readFromCache(cache, address, 8);
			retVal.data = 0;
			for (int i = 0; i < 8; i++) {
				retVal.data = (retVal.data << 8 ) | readData[i];
			}
			free(readData);
		}
		retVal.success = true;
	}
	return retVal;
}
