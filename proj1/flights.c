/* 
 * CS61C Summer 2013
 * Name1: Matthew Owen
 * Login1: cs61c-agj
 * Name2: Xintong (Robert) Wen
 * Login2: cs61c-ace
 * hello
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "flights.h"
#include "timeHM.h"

struct flightSys {
    /* Stores airport as double-ended linked list to make addAirport constant time */
    airport_t* firstAirport;
    airport_t* lastAirport;
};

struct airport {
    char* name;
    /* Airports as linked lists, storing pointer to flight linked list */
    airport_t* nextAirport;
    flight_t* firstFlight;
};

struct flight {
    airport_t* dest;
    timeHM_t dep;
    timeHM_t arr;
    /* Flights stored as linked lists */
    flight_t* nextFlight;
    int c;
};

/*
   This should be called if memory allocation failed.
 */
static void allocation_failed() {
    fprintf(stderr, "Out of memory.\n");
    exit(EXIT_FAILURE);
}


/*
   Creates and initializes a flight system, which stores the flight schedules of several airports.
   Returns a pointer to the system created.
 */
flightSys_t* createSystem() {
    flightSys_t* sys = (flightSys_t*) malloc(sizeof(flightSys_t));
    if (sys == NULL) {
        allocation_failed();
        return NULL;
    } else {
        /* Defaults to having a null set of airports upon creation */
        sys->firstAirport = NULL;
        sys->lastAirport = NULL;
        return sys;
    }
}


/* Given a destination airport, a departure and arrival time, and a cost, return a pointer to new flight_t. Note that this pointer must be available to use even
   after this function returns. (What does this mean in terms of how this pointer should be instantiated)?
*/

flight_t* createFlight(airport_t* dest, timeHM_t dep, timeHM_t arr, int c) {
    if (dest != NULL) {
        flight_t *f = (flight_t *) malloc(sizeof(flight_t));
        if (f == NULL) {
            allocation_failed();
            return NULL;
        } else {
            f->dest = dest;
            f->dep = dep;
            f->arr = arr;
            f->c = c;
            f->nextFlight = NULL;
            return f;
        }
    }
//    printf("Cannot create flight with NULL destination\n");
//    return NULL;
}

/*
   Frees all memory associated with this system; that's all memory you dynamically allocated in your code.
 */
void deleteSystem(flightSys_t* s) {
    if (s != NULL) {
        airport_t* currAirport = s->firstAirport;
        /* Outer loop: iterate through airport linked list */
        while(currAirport != NULL) {
            airport_t* tempAirport = currAirport->nextAirport;
            flight_t* currFlight = currAirport->firstFlight;
            /* Inner loop: iterate through flight linked list inside airport */
            while (currFlight != NULL) {
                flight_t* tempFlight = currFlight->nextFlight;
                free(currFlight);
                currFlight = tempFlight;
            }
            free(currAirport->name);
            free(currAirport);
            currAirport = tempAirport;
        }
        free(s);
    }
//    else {
//        printf("Cannot delete NULL flight system\n");
//    }
}


/*
   Adds a airport with the given name to the system. You must copy the string and store it.
   Do not store "name" (the pointer) as the contents it point to may change.
 */
void addAirport(flightSys_t* s, char* name) {
    if (s != NULL && name != NULL) {
        airport_t* newAirport = (airport_t*) malloc(sizeof(airport_t));
        if (newAirport == NULL) {
           allocation_failed();
        } else {
            newAirport->name = (char *) malloc(strlen(name) * sizeof(char) + 1);
            strcpy(newAirport->name, name);
            /* Defaults to size 1 linked list */
            newAirport->nextAirport = NULL;
            /* Flight schedule of airport defaults to null */
            newAirport->firstFlight = NULL;
            /* Adds new airport to system's linked list of airports */
            if (s->firstAirport == NULL) {
                s->firstAirport = newAirport;
                s->lastAirport = newAirport;
            } else {
                airport_t* last = s->lastAirport;
                last->nextAirport = newAirport;
                s->lastAirport = newAirport;
            }
        }
    }
//    else {
//        printf("Cannot add airport from NULL name or to NULL flight system\n");
//    }
}


/*
   Returns a pointer to the airport with the given name.
   If the airport doesn't exist, return NULL.
 */
airport_t* getAirport(flightSys_t* s, char* name) {
    if (s != NULL && name != NULL) {
        airport_t* curr = s->firstAirport;
        while (curr != NULL) {
            if (strcmp(curr->name, name) == 0) {
                return curr;
            } else {
                curr = curr->nextAirport;
            }
        }
        return NULL;
    }
}


/*
   Print each airport name in the order they were added through addAirport, one on each line.
   Make sure to end with a new line. You should compare your output with the correct output
   in flights.out to make sure your formatting is correct.
 */
void printAirports(flightSys_t* s) {
    if (s != NULL) {
        airport_t* curr = s->firstAirport;
        while (curr != NULL) {
            printf("%s\n", curr->name);
            curr = curr->nextAirport;
        }
    }
}


/*
   Adds a flight to src's schedule, stating a flight will leave to dst at departure time and arrive at arrival time.
 */
void addFlight(airport_t* src, airport_t* dst, timeHM_t* departure, timeHM_t* arrival, int cost) {
    if (src != NULL && dst != NULL && departure != NULL && arrival != NULL) {
        flight_t* newFlight = createFlight(dst, *departure, *arrival, cost);
        if (src->firstFlight == NULL) {
            src->firstFlight = newFlight;
        } else {
            flight_t* curr = src->firstFlight->nextFlight, *prev = src->firstFlight;
            /* Iterates to the end of airport src's flight schedule, adds flight */
            while (curr != NULL && isAfter(departure, &(curr->dep))) {
                curr = curr->nextFlight;
                prev = prev->nextFlight;
            }
            newFlight->nextFlight = curr;
            prev->nextFlight = newFlight;
        }
    }
//    else {
//        printf("Cannot add flight from NULL inputs\n");
//    }
}


/*
   Prints the schedule of flights of the given airport.

   Prints the airport name on the first line, then prints a schedule entry on each 
   line that follows, with the format: "destination_name departure_time arrival_time $cost_of_flight".

   You should use printTime (look in timeHM.h) to print times, and the order should be the same as 
   the order they were added in through addFlight. Make sure to end with a new line.
   You should compare your output with the correct output in flights.out to make sure your formatting is correct.
 */
void printSchedule(airport_t* s) {
    if (s != NULL) {
        printf("AIRPORT: %s\n", s->name);
        flight_t* curr = s->firstFlight;
        while (curr != NULL) {
            printf("%s ", curr->dest->name);
            printTime(&(curr->dep));
            printf(" ");
            printTime(&(curr->arr));
            printf(" ");
            printf("$%d", curr->c);
            printf("\n");
            curr = curr->nextFlight;
        }
    }
//    else {
//        printf("Cannot print schedule for NULL airport\n");
//    }
}


/*
   Given a src and dst airport, and the time now, finds the next flight to take based on the following rules:
   1) Finds the cheapest flight from src to dst that departs after now.
   2) If there are multiple cheapest flights, take the one that arrives the earliest.

   If a flight is found, you should store the flight's departure time, arrival time, and cost in departure, arrival, 
   and cost params and return true. Otherwise, return false. 

   Please use the function isAfter() from time.h when comparing two timeHM_t objects.
 */
bool getNextFlight(airport_t* src, airport_t* dst, timeHM_t* now, timeHM_t* departure, timeHM_t* arrival, int* cost) {
    if (src != NULL && dst != NULL && now != NULL && departure != NULL && arrival != NULL && cost != NULL) {
        if (src->firstFlight != NULL){
            flight_t* curr = src->firstFlight;
            flight_t* nextFlight = NULL;
            int cheapest = 0x7FFFFFFF;
            timeHM_t earliest;
            /* Iterate through flight schedule of src, keeping track of the cheapest flight
             * after designated time, and uses earlier departure as tie-breaker*/
            while (curr != NULL) {
                if (curr->dest == dst && isAfter(&(curr->dep), now)) {
                    if (cheapest > curr->c) {
                        nextFlight = curr;
                        cheapest = curr->c;
                        earliest = curr->arr;
                    } else if (cheapest == curr->c && isAfter(&earliest, &(curr->arr))) {
                        nextFlight = curr;
                        cheapest = curr->c;
                        earliest = curr->arr;
                    }
                }
                curr = curr->nextFlight;
            }
            if (nextFlight == NULL){
                return false;
            } else {
                *departure = nextFlight->dep;
                *arrival = nextFlight->arr;
                *cost = nextFlight->c;
                return true;
            }

        }
    }
    return false;
}

/* Given a list of flight_t pointers (flight_list) and a list of destination airport names (airport_name_list), first confirm that it is indeed possible to take these sequences of flights,
   (i.e. be sure that the i+1th flight departs after or at the same time as the ith flight arrives) (HINT: use the isAfter and isEqual functions).
   Then confirm that the list of destination airport names match the actual destination airport names of the provided flight_t struct's.
   sz tells you the number of flights and destination airport names to consider. Be sure to extensively test for errors (i.e. if you encounter NULL's for any values that you might expect to
   be non-NULL, return -1).

   Return from this function the total cost of taking these sequence of flights. If it is impossible to take these sequence of flights, if the list of destination airport names
   doesn't match the actual destination airport names provided in the flight_t struct's, or if you run into any errors mentioned previously or any other errors, return -1.
*/
int validateFlightPath(flight_t** flight_list, char** airport_name_list, int sz) {
    if (flight_list == NULL || airport_name_list == NULL) {
        return -1;
    }
    if (sz == 0) {
        return -1;
    }

    /* Checks that flight list is possible by comparing previous arrival and next departure */
    if (sz > 1) {
        flight_t* prevFlight = flight_list[0];
        flight_t* currFlight;
        for (int i = 1; i < sz; i++) {
            currFlight = flight_list[i];
            /* If either is null, the size mismatches the length of the flight list */
            if (currFlight == NULL || prevFlight == NULL) {
                return -1;
            }
            if (isAfter(&(prevFlight->arr), &(currFlight->dep))) {
                return -1;
            }
            prevFlight = currFlight;
        }
    }

    /* Returns false if the order of the flights and the list of destination airports are misaligned */
    for (int j = 0; j < sz; j++) {
        if (flight_list[j] == NULL || airport_name_list[j] == NULL) {
            return -1;
        }
        if (strcmp(((flight_list[j])->dest->name), airport_name_list[j]) != 0) {
            return -1;
        }
    }

    /* Accumulates cost */
    int cost = 0;
    for (int k = 0; k < sz; k++) {
        cost += (flight_list[k])->c;
    }
    return cost;
}
