# CS 61C Summer 2016 Project 2-2 
# string.s

#==============================================================================
#                              Project 2-2 Part 1
#                               String README
#==============================================================================
# In this file you will be implementing some utilities for manipulating strings.
# The functions you need to implement are:
#  - strlen()
#  - strncpy()
#  - copy_of_str()
# Test cases are in linker-tests/test_string.s
#==============================================================================

.data
newline:	.asciiz "\n"
tab:	.asciiz "\t"

.text
#------------------------------------------------------------------------------
# function strlen()
#------------------------------------------------------------------------------
# Arguments:
#  $a0 = string input
#
# Returns: the length of the string
#------------------------------------------------------------------------------
strlen:
	# YOUR CODE HERE
	addiu $v0 $0 0 # sum = 0
strlenloop:
	lb $t0 0($a0) # load first char of a0 into t0
	beq $t0 $0 strlenret # if null terminator, finish
	addiu $v0 $v0 1 # else add 1 to sum
	addiu $a0 $a0 1 # advance string pointer to next byte
	j strlenloop # loop
strlenret:
	jr $ra

#------------------------------------------------------------------------------
# function strncpy()
#------------------------------------------------------------------------------
# Arguments:
#  $a0 = pointer to destination array
#  $a1 = source string
#  $a2 = number of characters to copy
#
# Returns: the destination array
#------------------------------------------------------------------------------
strncpy:
	# YOUR CODE HERE
	addiu $t0 $a0 0 # t0 = temp pointer to destination array
strncpyloop:
	lb $t1 0($a1) # t1 = first char of a1
	beq $a2 $0 strncpyret # if number of chars to copy is 0, finish
	sb $t1 0($a0) # store char of a1 into a0
	addiu $a0 $a0 1 # advance a0, a1 pointers to next char
	addiu $a1 $a1 1
	addiu $a2 $a2 -1 # reduce chars to copy by 1
	j strncpyloop # loop
strncpyret:
	addiu $v0 $t0 0 # bring back temp destination to v0
	jr $ra

#------------------------------------------------------------------------------
# function copy_of_str()
#------------------------------------------------------------------------------
# Creates a copy of a string. You will need to use sbrk (syscall 9) to allocate
# space for the string. strlen() and strncpy() will be helpful for this function.
# In MARS, to malloc memory use the sbrk syscall (syscall 9). See help for details.
#
# Arguments:
#   $a0 = string to copy
#
# Returns: pointer to the copy of the string
#------------------------------------------------------------------------------
copy_of_str:
	addiu $sp $sp -12
	sw $s0 0($sp)
	sw $s1 4($sp)
	sw $ra 8($sp)
	# YOUR CODE HERE
	addiu $s0 $a0 0 # s0 to hold a0 
	jal strlen
	addiu $a0 $v0 1 # a0 is strlen + 1 for null terminator
	addiu $s1 $a0 0 # store s1 as length 
	addiu $v0 $0 9 # set v0 = 9 for malloc syscall,
	syscall # now v0 is address of malloc'd space
	addiu $a2 $s1 0 # a2 is strlen + 1 to copy
	addiu $a1 $s0 0 # a1 back to src string to copy
	addiu $a0 $v0 0 # malloc'd space is destination array
	jal strncpy # v0 is destination containing copied source
	lw $s0 0($sp)
	lw $s1 4($sp)
	lw $ra 8($sp)
	addiu $sp $sp 12
	jr $ra

###############################################################################
#                 DO NOT MODIFY ANYTHING BELOW THIS POINT                       
###############################################################################

#------------------------------------------------------------------------------
# function streq() - DO NOT MODIFY THIS FUNCTION
#------------------------------------------------------------------------------
# Arguments:
#  $a0 = string 1
#  $a1 = string 2
#
# Returns: 0 if string 1 and string 2 are equal, -1 if they are not equal
#------------------------------------------------------------------------------
streq:
	beq $a0, $0, streq_false	# Begin streq()
	beq $a1, $0, streq_false
streq_loop:
	lb $t0, 0($a0)
	lb $t1, 0($a1)
	addiu $a0, $a0, 1
	addiu $a1, $a1, 1
	bne $t0, $t1, streq_false
	beq $t0, $0, streq_true
	j streq_loop
streq_true:
	li $v0, 0
	jr $ra
streq_false:
	li $v0, -1
	jr $ra			# End streq()

#------------------------------------------------------------------------------
# function dec_to_str() - DO NOT MODIFY THIS FUNCTION
#------------------------------------------------------------------------------
# Convert a number to its unsigned decimal integer string representation, eg.
# 35 => "35", 1024 => "1024". 
#
# Arguments:
#  $a0 = int to write
#  $a1 = character buffer to write into
#
# Returns: the number of digits written
#------------------------------------------------------------------------------
dec_to_str:
	li $t0, 10			# Begin dec_to_str()
	li $v0, 0
dec_to_str_largest_divisor:
	div $a0, $t0
	mflo $t1		# Quotient
	beq $t1, $0, dec_to_str_next
	mul $t0, $t0, 10
	j dec_to_str_largest_divisor
dec_to_str_next:
	mfhi $t2		# Remainder
dec_to_str_write:
	div $t0, $t0, 10	# Largest divisible amount
	div $t2, $t0
	mflo $t3		# extract digit to write
	addiu $t3, $t3, 48	# convert num -> ASCII
	sb $t3, 0($a1)
	addiu $a1, $a1, 1
	addiu $v0, $v0, 1
	mfhi $t2		# setup for next round
	bne $t2, $0, dec_to_str_write
	jr $ra			# End dec_to_str()
